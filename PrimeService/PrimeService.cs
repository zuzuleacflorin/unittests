﻿using System;

namespace Prime.Services
{
    public class PrimeService
    {
        public bool IsPrime(int candidate)
        {
            if (candidate < 2)
            {
                return false;
            }
            throw new NotImplementedException("Not fully implemented.");
        }

        public int MaxConsecutiveLatinLetters(string input)
        {
            if (string.IsNullOrEmpty(input))
                throw new ArgumentException("Input string cannot be null or empty.");

            int max = 1;
            int current = 1;

            for (int i = 1; i < input.Length; i++)
            {
                if (char.IsLetter(input[i]) && char.IsLetter(input[i - 1]))
                {
                    if (char.ToUpper(input[i]) == char.ToUpper(input[i - 1]))
                    {
                        current++;
                        max = Math.Max(max, current);
                    }
                    else
                    {
                        current = 1;
                    }
                }
                else if (!char.IsLetter(input[i]))
                {
                    throw new ArgumentException("Input contains non-Latin characters.");
                }
            }

            return max;
        }

        public int MaxConsecutiveIdenticalDigits(string input)
        {
            if (string.IsNullOrEmpty(input) || !IsAllDigits(input))
            {
                return 0;
            }

            int maxCount = 1;
            int currentCount = 1;

            for (int i = 1; i < input.Length; i++)
            {
                if (input[i] == input[i - 1])
                {
                    currentCount++;
                }
                else
                {
                    if (currentCount > maxCount)
                    {
                        maxCount = currentCount;
                    }
                    currentCount = 1;
                }
            }
            if (currentCount > maxCount)
            {
                maxCount = currentCount;
            }

            return maxCount;
        }

        private bool IsAllDigits(string input)
        {
            foreach (char c in input)
            {
                if (!char.IsDigit(c))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
