﻿using Xunit;
using Prime.Services;
using System.Diagnostics.Metrics;
using Microsoft.VisualStudio.TestPlatform.TestHost;

namespace Prime.UnitTests.Services
{
    public class PrimeService_IsPrimeShould
    {
        private readonly PrimeService _primeService;

        public PrimeService_IsPrimeShould()
        {
            _primeService = new PrimeService();
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        [InlineData(1)]
        public void IsPrime_ValuesLessThan2_ReturnFalse(int value)
        {
            var result = _primeService.IsPrime(value);

            Assert.False(result, $"{value} should not be prime");
        }


        [Theory]
        [InlineData("abcd", 1)]
        [InlineData("aabbbccdeeee", 4)]
        [InlineData("aabbbccd", 3)]
        public void MaxConsecutiveIdenticalLetters_Tests(string input, int expected)
        {
            int result = _primeService.MaxConsecutiveLatinLetters(input);

            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData("")]
        [InlineData("a1BcD")]
        [InlineData("Hello!")]
        [InlineData(null)]
        public void MaxConsecutiveLatinLetters_ShouldThrowArgumentException(string input)
        {
            Assert.Throws<ArgumentException>(() => _primeService.MaxConsecutiveLatinLetters(input));
        }


        [Theory]
        [InlineData("", 0)]
        [InlineData("123456", 1)]
        [InlineData("112233", 2)]
        [InlineData("11122333", 3)]
        [InlineData("112223333444455555", 5)]
        [InlineData("abc123", 0)]       
        [InlineData("123a456", 0)]
        [InlineData("!@#$%^", 0)]
        public void MaxConsecutiveIdenticalDigits(string input, int expected)
        {
            int result = _primeService.MaxConsecutiveIdenticalDigits(input);

            Assert.Equal(expected, result);
        }


    }
}
